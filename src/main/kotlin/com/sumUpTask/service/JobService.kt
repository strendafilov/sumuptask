package com.sumUpTask.service

import com.sumUpTask.exceptions.InvalidTaskNameException
import com.sumUpTask.exceptions.TasksInCycleException
import com.sumUpTask.model.RequestTaskModel
import com.sumUpTask.model.ResponseTaskModel
import com.sumUpTask.request.JobRequest
import java.lang.StringBuilder
import javax.inject.Singleton

// THE ACTUAL JOB IS DONE BY THIS SERVICE
// IT HAS TWO MAIN METHODS - ONE OF IT GENERATES THE SORTED ARRAY OF
@Singleton
class JobService {
    //THIS METHOD IS CALLED BY THE CONTROLLER TO RETURN THE ARRAY OF ResponseTaskModels
    fun createJsonObject(request: JobRequest): ArrayList<ResponseTaskModel> {
        return getResponseTaskModels(request.tasks)
    }

    //THIS METHOD IS CALLED BY THE CONTROLLER TO RETURN BASH SCRIPT, THAT IS EXECUTABLE
    fun getBashScript(request: JobRequest): String {
        val sorted = getResponseTaskModels(request.tasks)
        return generateBashScriptSortedResponse(sorted)
    }

    // THE METHOD ITERATES THROUGH THE COMMANDS OF THE ORDERED ARRAY OF ResponseTaskModels AND CONCATENATES THE COMMANDS
    // IN ONE STRING, WHERE EACH COMMAND IS SEPARATED ON NEW LINE
    private fun generateBashScriptSortedResponse(sorted: java.util.ArrayList<ResponseTaskModel>): String {
        val builder = StringBuilder()
        sorted.forEach {
            builder.append(it.command)
            builder.append("\n")
        }

        return builder.toString()
    }

    // THIS METHODS EXTRACTS THE ResponseTaskModel ARRAY FROM THE REQUEST'S RequestTaskModel
    // I AM USING TWO BOOLEAN ARRAYS TO INDICATE THE GLOBALLY VISITED OBJECTS BY IDX
    // AND TO BE ABLE TO FOLLOW EACH TASK'S PREREQUISITES FOR CYCLIC DEPENDENCIES
    // THE FINAL RESULT IS KEPT IN THE sorted ARRAY
    // WE ALSO CHECK FOR DUPLICATED TASK NAMES, AND RETURN A BAD REQUEST IF THIS HAPPENS
    private fun getResponseTaskModels(tasks: Array<RequestTaskModel>): ArrayList<ResponseTaskModel> {
        val tasksCount = tasks.count()
        val visited = BooleanArray(tasksCount) { false }
        val onCurrentPath = BooleanArray(tasksCount) { false }
        val sorted = ArrayList<ResponseTaskModel>()

        tasks.forEachIndexed { index, task ->
            val tasksWithSameNameCount = tasks.filter { it.name == task.name }.count()
            if (tasksWithSameNameCount > 1) {
                throw InvalidTaskNameException("Task name ${task.name} duplicated")
            }

            if (!visited[index]) {
                sortRequestTasks(tasks, visited, onCurrentPath, index, task, sorted)
            }
        }

        return sorted
    }

    // THIS IS A RECURSIVELY CALLED FUNCTION, THAT IS USED TO CHECK IF THE CURRENTLY CHECKED ELEMENT
    // IS ALREADY TAKEN OR IF IT HAS BEEN CHECKED AS A PART OF THIS FLOW, SO WE'LL KNOW IF THERE IS
    // A CYCLIC REFERENCE.
    // WE ALSO CHECK IF ANY OF THE REFERENCES LEADS US TO A TASK, THAT IS NOT PRESENTED IN THE CURRENT JOB
    private fun sortRequestTasks(
        tasks: Array<RequestTaskModel>,
        visited: BooleanArray,
        onCurrentPath: BooleanArray,
        index: Int,
        task: RequestTaskModel,
        sorted: ArrayList<ResponseTaskModel>
    ) {
        visited[index] = true
        onCurrentPath[index] = true

        task.requires?.forEach { taskName ->
            val requiredTask: RequestTaskModel = tasks.firstOrNull { it.name == taskName }
                ?: throw InvalidTaskNameException("Task name $taskName is invalid in the current context")

            val taskIndex = tasks.indexOf(requiredTask)
            if (onCurrentPath[taskIndex]) {
                throw TasksInCycleException("Tasks are in cycle and can not proceed")
            }

            if (!visited[taskIndex]) {
                sortRequestTasks(tasks, visited, onCurrentPath, taskIndex, requiredTask, sorted)
            }
        }

        onCurrentPath[index] = false
        sorted.add(ResponseTaskModel(task.name, task.command))
    }
}
