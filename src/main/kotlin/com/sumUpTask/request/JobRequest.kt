package com.sumUpTask.request

import com.sumUpTask.model.RequestTaskModel

data class JobRequest(var tasks: Array<RequestTaskModel>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as JobRequest

        if (!tasks.contentEquals(other.tasks)) return false

        return true
    }

    override fun hashCode(): Int {
        return tasks.contentHashCode()
    }
}
