package com.sumUpTask.exceptions

import com.sumUpTask.response.ExceptionResponse
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton

@Produces
@Singleton
class TasksInCycleExceptionHandler: ExceptionHandler<TasksInCycleException, HttpResponse<ExceptionResponse>> {
    override fun handle(request: HttpRequest<*>?, exception: TasksInCycleException?): HttpResponse<ExceptionResponse> =
        HttpResponse.badRequest(ExceptionResponse(message = exception?.message))
}