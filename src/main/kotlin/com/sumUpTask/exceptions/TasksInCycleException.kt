package com.sumUpTask.exceptions

import java.lang.RuntimeException

class TasksInCycleException(message: String) : RuntimeException(message)