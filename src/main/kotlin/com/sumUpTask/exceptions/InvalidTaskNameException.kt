package com.sumUpTask.exceptions

import java.lang.RuntimeException

class InvalidTaskNameException(message: String): RuntimeException(message)