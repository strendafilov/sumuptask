package com.sumUpTask.exceptions

import com.sumUpTask.response.ExceptionResponse
import io.micronaut.http.HttpRequest
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton
import io.micronaut.http.HttpResponse as HttpResponse

@Produces
@Singleton
class InvalidTaskNameExceptionHandler : ExceptionHandler<InvalidTaskNameException, HttpResponse<ExceptionResponse>> {
    override fun handle(
        request: HttpRequest<*>?,
        exception: InvalidTaskNameException?
    ): HttpResponse<ExceptionResponse> = HttpResponse.badRequest(ExceptionResponse(exception?.message))
}