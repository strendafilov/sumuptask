package com.sumUpTask.controller

import com.sumUpTask.model.ResponseTaskModel
import com.sumUpTask.request.JobRequest
import com.sumUpTask.service.JobService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*

@Controller("api/job")
class JobController(private val jobService: JobService) {
    // I HAVE ALLOWED MediaType.ALL SINCE THE DEFAULT MEDIA TYPE, THAT MICRONAUT ACCEPTS IS application/json
    // BUT THE DEFAULT MEDIA TYPE FOR THE CURL POST REQUESTS IS application/x-www-form-urlencoded
    // AND IF YOU INTENT TO USE SOME AUTO SCRIPTS AND COMMANDS LIKE THE ONE IN THE TASK
    // '$ curl -d @mytasks.json http://localhost:4000/... | bash'
    // IT WILL NOT WORK
    @Post
    @Consumes(MediaType.ALL)
    fun createJsonObject(request: JobRequest): HttpResponse<ArrayList<ResponseTaskModel>> {
        return HttpResponse.ok(jobService.createJsonObject(request))
    }

    @Post("script")
    @Consumes(MediaType.ALL)
    fun createBashScript(@Body request: JobRequest): HttpResponse<String> {
        return HttpResponse.ok(jobService.getBashScript(request))
    }
}