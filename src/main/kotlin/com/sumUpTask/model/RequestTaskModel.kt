package com.sumUpTask.model

// I am using data class, that will contain a single task
// since the name and the command are not supposed to be changed
// the class properties are immutable.


// I've separated the tasks models for the request and the response
// since this way they can be extended and extending the request task model
// will not affect the response task model and vice versa

data class RequestTaskModel(val name: String, val command: String, val requires: Array<String>? = null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RequestTaskModel

        if (name != other.name) return false
        if (command != other.command) return false
        if (requires != null) {
            if (other.requires == null) return false
            if (!requires.contentEquals(other.requires)) return false
        } else if (other.requires != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + command.hashCode()
        result = 31 * result + (requires?.contentHashCode() ?: 0)
        return result
    }
}
