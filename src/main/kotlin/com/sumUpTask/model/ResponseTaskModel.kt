package com.sumUpTask.model

// I've separated the tasks models for the request and the response
// since this way they can be extended and extending the request task model
// will not affect the response task model and vice versa
data class ResponseTaskModel(val name: String, val command: String)