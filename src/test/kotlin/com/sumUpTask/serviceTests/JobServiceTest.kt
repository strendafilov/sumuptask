package com.sumUpTask.serviceTests

import com.sumUpTask.exceptions.InvalidTaskNameException
import com.sumUpTask.exceptions.TasksInCycleException
import com.sumUpTask.model.RequestTaskModel
import com.sumUpTask.model.ResponseTaskModel
import com.sumUpTask.request.JobRequest
import com.sumUpTask.service.JobService
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


@MicronautTest
class JobServiceTest {

    @Test
    fun getBashScript_ShouldReturnCommandsOnNewLines() {
        val service = JobService()

        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2")
            )
        )

        val result = service.getBashScript(request)
        Assertions.assertEquals("command-1\ncommand-2\n", result)
    }

    @Test
    fun getBashScript_shouldTrowExceptionWhenThereIsInvalidRequirement() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2", arrayOf("task-5")),
                RequestTaskModel("task-3", "command-3")
            )
        )

        val exception = assertThrows<InvalidTaskNameException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Task name task-5 is invalid in the current context", exception.message)
    }

    @Test
    fun getBashScript_shouldTrowExceptionWhenThereIsDuplicatedTaskName() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-1", "command-2", arrayOf("task-3")),
                RequestTaskModel("task-3", "command-3")
            )
        )

        val exception = assertThrows<InvalidTaskNameException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Task name task-1 duplicated", exception.message)
    }

    @Test
    fun getBashScript_shouldTrowExceptionWhenTasksAreInCycle() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2", arrayOf("task-3")),
                RequestTaskModel("task-3", "command-3", arrayOf("task-2"))
            )
        )

        val exception = assertThrows<TasksInCycleException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Tasks are in cycle and can not proceed", exception.message)
    }

    @Test
    fun createJsonObject_ShouldReturnProperArrayOfTasks() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2", arrayOf("task-3")),
                RequestTaskModel("task-3", "command-3")
            )
        )

        val expectedResult = arrayOf(
            ResponseTaskModel("task-1", "command-1"),
            ResponseTaskModel("task-3", "command-3"),
            ResponseTaskModel("task-2", "command-2")
        )

        val actualResult = service.createJsonObject(request)

        actualResult.forEachIndexed { index, singleResult ->
            Assertions.assertEquals(expectedResult[index].name, singleResult.name)
            Assertions.assertEquals(expectedResult[index].command, singleResult.command)
        }
    }

    @Test
    fun createJsonObject_shouldTrowExceptionWhenThereIsInvalidRequirement() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2", arrayOf("task-5")),
                RequestTaskModel("task-3", "command-3")
            )
        )

        val exception = assertThrows<InvalidTaskNameException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Task name task-5 is invalid in the current context", exception.message)
    }

    @Test
    fun createJsonObject_shouldTrowExceptionWhenThereIsDuplicatedTaskName() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-1", "command-2", arrayOf("task-3")),
                RequestTaskModel("task-3", "command-3")
            )
        )

        val exception = assertThrows<InvalidTaskNameException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Task name task-1 duplicated", exception.message)
    }

    @Test
    fun createJsonObject_shouldTrowExceptionWhenTasksAreInCycle() {
        val service = JobService()
        val request = JobRequest(
            arrayOf(
                RequestTaskModel("task-1", "command-1"),
                RequestTaskModel("task-2", "command-2", arrayOf("task-3")),
                RequestTaskModel("task-3", "command-3", arrayOf("task-2"))
            )
        )

        val exception = assertThrows<TasksInCycleException> {
            service.createJsonObject(request)
        }

        Assertions.assertEquals("Tasks are in cycle and can not proceed", exception.message)
    }
}