**The service has two endpoints**

**http://localhost:4000/api/job**

**http://localhost:4000/api/job/script**

**The first one will return a json object as in the task description**

**The second one will return a script, that can be executed in the way, described in the task**

I've also added unit tests for the service functionality.